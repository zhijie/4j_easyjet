#include "Util.h"

void Ana4j(){
  auto t = getTree("OUT.root","AnalysisMiniTree");
//  t->Show(0);
  VF *tj_pt = nullptr;
  VF *rj_pt = nullptr;
  TBranch *b1 = nullptr;
  TBranch *b2 = nullptr;
  t->SetBranchAddress("truthjet_antikt4_pt",&tj_pt,&b1);
  t->SetBranchAddress("recojet_antikt4PFlow_NOSYS_pt",&rj_pt,&b2);
//  for (int i = 0; i < t->GetEntries(); ++i){
    t->GetEntry(0);
//    std::cout << "size = " << rj_pt->size() << std::endl;
    for (auto pt:*tj_pt){
      std::cout << "tj_pt = " << pt << std::endl;
    }
    for (auto pt:*rj_pt){
      std::cout << "rj_pt = " << pt << std::endl;
    }
//  }
}