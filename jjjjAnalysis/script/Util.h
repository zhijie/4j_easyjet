#ifndef UTIL_H
#define UTIL_H

typedef TString Str;
typedef std::vector<Str> VS;
typedef std::vector<double> VD;
typedef std::vector<float> VF;
typedef std::vector<int> VI;
typedef std::vector<TH1D*> VH;

map<Str,TFile*> mapf;

TFile *getFile(Str fn) {
  TFile *f = TFile::Open(fn);
  return f;
}

TTree *getTree(TFile *f, Str tn) {
  TTree *t = (TTree*)f->Get(tn);
  return t;
}

TTree *getTree(Str fn, Str tn) {
  return getTree(getFile(fn), tn);
}
#endif