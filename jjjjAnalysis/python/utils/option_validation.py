def validate_jjjj_analysis_prerequisites(flags):
    if flags.Analysis.do_jjjj:
        assert flags.Analysis.do_small_R_jets
        assert not flags.Analysis.disable_calib
