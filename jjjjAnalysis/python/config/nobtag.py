from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from EasyjetHub.output.ttree.selected_objects import (
    get_selected_objects_branches,
)

from EasyjetHub.algs.event_counter_config import event_counter_cfg
#from jjjjAnalysis.config.histograms import histograms_cfg

def nobtag_cfg(flags):
    cfg = ComponentAccumulator()

    smalljetkey = flags.Analysis.container_names.output.reco4PFlowJet
    if flags.Analysis.do_jjjj:
        cfg.merge(
            jjjj_cfg(
                flags,
                smalljetkey=smalljetkey.replace("%SYS%", "NOSYS"),
            )
        )
        cfg.merge(event_counter_cfg("n_resolved"))

    #cfg.merge(histograms_cfg(flags))

    return cfg


def jjjj_cfg(flags, smalljetkey):
    cfg = ComponentAccumulator()

    # this is a nobtag analysis chain
    btag_wps = []
    # get the 4 leading small R jets
    cfg.addEventAlgo(
        CompFactory.Easyjet.JetSelectorAlg(
            "SmallJetSelectorAlg",
            containerInKey=smalljetkey,
            containerOutKey="jjjjAnalysisJets",
            minPt=20e3,
            maxEta=4.5,
            pTsort=1,
            truncateAtAmount=4,  # -1 means keep all
            minimumAmount=4,  # -1 means ignores this
            checkOR=flags.Analysis.do_overlap_removal,
        )
    )

    ### label jets
    ### jet1, jet2, jet3, jet4
    cfg.addEventAlgo(
        CompFactory.jjjj.JetLabellingAlg(
            "JetLabellingAlg",
            containerInKey="jjjjAnalysisJets",
            containerOutKey="labelledjjjjAnalysisJets",
            labellingStrategy="pTSorting",  # default pT sorting
        )
    )

    ### calculate flat vars
    cfg.addEventAlgo(
        CompFactory.jjjj.BaselineVarsjjjjAlg(
            "FinalVarsjjjjAlg",
            smallRContainerInKey="labelledjjjjAnalysisJets",
        )
    )

    cfg.addEventAlgo(
        CompFactory.Easyjet.EventInfoGlobalAlg(
            isMC=flags.Input.isMC,
            Years=flags.Analysis.Years,
        )
    )

    return cfg


def jjjj_branches(flags):
    branches = []

    # These are the variables always saved with the objects selected by the analysis
    # This is tunable with the flags amount and variables
    # in the object configs.
    branches += get_selected_objects_branches(flags, "jjjj")

    vars = [
        "nJets",
        "j1_pT",
        "j2_pT",
        "j1j2_dphi",
    ]

    for var in vars:
        branches += [
            f"EventInfo.{var} -> {var}"
        ]

    ### to check why it is crashing
    #branches += ["EventInfo.dataTakingYear -> dataTakingYear"]

    return branches
