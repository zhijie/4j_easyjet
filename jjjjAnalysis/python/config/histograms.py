from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def histograms_cfg(flags, jets=None, out_path=None):
    if not jets:
        jets = flags.Analysis.container_names.output.reco4PFlowJet
    cfg = ComponentAccumulator()
    if path := out_path or flags.Analysis.output_hists:
        output = CompFactory.H5FileSvc(path=str(path))
        cfg.addService(output)
        cfg.addEventAlgo(
            CompFactory.jjjj.JetBoostHistogramsAlg(
                name="JetBoostHistogramsAlg",
                jetsIn=str(jets),
                output=output,
            )
        )

    return cfg
