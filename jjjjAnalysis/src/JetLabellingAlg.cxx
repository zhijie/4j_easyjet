/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Frederic Renner ---> Antonio Giannini

#include "JetLabellingAlg.h"
#include "AthContainers/AuxElement.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "xAODJet/JetContainer.h"
#include <AthContainers/ConstDataVector.h>

namespace jjjj
{
  JetLabellingAlg ::JetLabellingAlg(const std::string &name,
                                ISvcLocator *pSvcLocator)
      : AthHistogramAlgorithm(name, pSvcLocator)
  {
    declareProperty("labellingStrategy", m_labellingStrategy);
  }

  StatusCode JetLabellingAlg ::initialize()
  {
    ATH_CHECK(m_containerInKey.initialize());
    ATH_CHECK(m_containerOutKey.initialize());

    return StatusCode::SUCCESS;
  }

  StatusCode JetLabellingAlg ::execute()
  {
    // container we read in
    SG::ReadHandle<ConstDataVector<xAOD::JetContainer>> inContainer(
        m_containerInKey);
    ATH_CHECK(inContainer.isValid());

    // fill workContainer with "views" of the inContainer
    // see TJ's tutorial for this
    auto workContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
        inContainer->begin(), inContainer->end(), SG::VIEW_ELEMENTS);

    // this assumes that container is pt sorted (use the JetSelectorAlg for
    // this) and checks if we have at least 4 jets otherwise exit this alg
    if (m_labellingStrategy == "pTSorting" && workContainer->size() >= 4)
    {
      // need to do nothing
      // pT sorting is implemented in the jet handler
    }
    else if (m_labellingStrategy == "AnotherAlgorithm" && workContainer->size() >= 4)
    {
      // here the implementation
    }

    // write to EventStore
    SG::WriteHandle<ConstDataVector<xAOD::JetContainer>> Writer(
        m_containerOutKey);
    ATH_CHECK(Writer.record(std::move(workContainer)));

    return StatusCode::SUCCESS;
  }
}