/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Frederic Renner ---> Antonio Giannini

#include "AthContainers/AuxElement.h"
#include "BaselineVarsjjjjAlg.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <AthContainers/ConstDataVector.h>
#include <xAODJet/JetContainer.h>

namespace jjjj
{
  BaselineVarsjjjjAlg ::BaselineVarsjjjjAlg(const std::string &name,
                                                    ISvcLocator *pSvcLocator)
      : AthHistogramAlgorithm(name, pSvcLocator)
  {
    //declareProperty("bTagWP", m_bTagWP);
  }

  StatusCode BaselineVarsjjjjAlg ::initialize()
  {
    ATH_CHECK(m_smallRContainerInKey.initialize());
    ATH_CHECK(m_EventInfoKey.initialize());

    // make decorators
    for (const std::string& var : m_vars)
    {
      std::string deco_var = var;
      SG::AuxElement::Decorator<float> deco(deco_var);
      m_decos.emplace(deco_var, deco);
    };
    return StatusCode::SUCCESS;
  }

  StatusCode BaselineVarsjjjjAlg ::execute()
  {
    // container we read in
    SG::ReadHandle<xAOD::EventInfo> eventInfo(m_EventInfoKey);
    ATH_CHECK(eventInfo.isValid());

    // set defaults
    for (const std::string& var : m_vars)
    {
      std::string deco_var = var;
      m_decos.at(deco_var)(*eventInfo) = -1.;
    };

    SG::ReadHandle<ConstDataVector<xAOD::JetContainer>> smallRjets(
        m_smallRContainerInKey);
    ATH_CHECK(smallRjets.isValid());

    ConstDataVector<xAOD::JetContainer> jets = *smallRjets;

    // assign variables values
    // according to jets multiplicity

    // clang-format off
    m_decos.at("nJets")(*eventInfo) = jets.size();
    if (jets.size() > 0){
      m_decos.at("j1_pT")(*eventInfo) = jets[0]->jetP4().pt();
    }
    if (jets.size() > 1){
      m_decos.at("j1j2_dphi")(*eventInfo) = xAOD::P4Helpers::deltaPhi(jets[0],jets[1]);
    }
    // clang-format on


    return StatusCode::SUCCESS;
  }
}
