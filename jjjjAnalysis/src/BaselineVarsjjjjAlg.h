/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

// Always protect against multiple includes!
#ifndef JJJJANALYSIS_FINALVARSJJJJALG
#define JJJJANALYSIS_FINALVARSJJJJALG

#include <AthenaBaseComps/AthHistogramAlgorithm.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

#include "AthContainers/ConstDataVector.h"

namespace jjjj
{

  /// \brief An algorithm for counting containers
  class BaselineVarsjjjjAlg final : public AthHistogramAlgorithm
  {
    /// \brief The standard constructor
public:
    BaselineVarsjjjjAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// \brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute() override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

private:
    // ToolHandle<whatever> handle {this, "pythonName", "defaultValue",
    // "someInfo"};

    SG::ReadHandleKey<ConstDataVector<xAOD::JetContainer>>
        m_smallRContainerInKey{this, "smallRContainerInKey", "",
                               "containerName to read"};
    SG::ReadHandleKey<xAOD::EventInfo> m_EventInfoKey{
        this, "EventInfoKey", "EventInfo", "EventInfo container to dump"};

    std::unordered_map<std::string, SG::AuxElement::Decorator<float>> m_decos;
    // clang-format off
    std::vector<std::string> m_vars{
      "nJets",
      "j1_pT",
      "j2_pT",
      "j1j2_dphi",
    };
    // clang-format on
  };
}

#endif
